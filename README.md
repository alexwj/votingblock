# VotingBlock
## Overview

This is the repository for VotingBlock, a EOSIO powered blockchain voting application.

## Usage

To use, start the Blockchain Node, start the Middleware Server, then start the Front end.  See the section READMEs for information on starting the various components.

## Blockchain Node

For more information see `./PollingStation/Blockchain/README.md`.

## Middleware Server

For more information see `./PollingStation/Server/README.md`.

## Front End

For more information see `./VotingBooth/front-end/README.md`.
