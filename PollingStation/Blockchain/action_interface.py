#!/usr/bin/env python3

from sys import argv as args
from subprocess import Popen as run

def help():
    print("Usage: " + args[0] + " ACTION <arg>")
    print("\tCommands:")
    print("\t\tstart - Action to start the election, takes 1 arg[s]")
    print("\t\tstop - Action to stop the election, takes 1 arg[s]")
    print("\t\tvote - Action to vote in the election, takes 1 arg[s]")
    print("\t\ttest_start - Tests start functionality, takes 0 arg[s]")
    print("\t\ttest_vote - Tests vote functionality, takes 0 args[s]")
    print("\t\ttest_stop - Tests stop functionality, takes 0 args[s]")
    exit(1)

def start(arg):
    run(["scripts/action_start.sh", arg])

def stop(arg):
    run(["scripts/action_stop.sh", arg])

def vote(arg):
    run(["scripts/action_vote.sh", arg])

def main():
    
    if len(args) < 2:
        help()

    if args[1] == "start" and len(args) == 3:
        start(args[2])

    elif args[1] == "stop" and len(args) == 3:
        stop(args[2])

    elif args[1] == "vote" and len(args) == 3:
        vote(args[2])

    elif args[1] == "test_start" and len(args) == 2:
        test_str = '["election","ewoJImVsZWN0aW9uX2lkIiA6ICJ1bmlxdWV0aXRsZSIsCgkiZWxlY3Rpb25fdGl0bGUiIDogIkh1bWFuIFJlYWRhYmxlIFRpdGxlIiwKCSJjaG9pY2VzIiA6IFsKCQl7CgkJCSJwb3NpdGlvbiIgOiAiV2hhdCBpcyBiZWluZyBzZWxlY3RlZCAiLAoJCQkib3B0aW9ucyIgOiBbCgkJCQkiT25lIG9wdGlvbiIsCgkJCQkiQW5vdGhlciBvcHRpb24iLAoJCQkJIkEgdGhpcmQgb3B0aW9uIgoJCQldLAoJCQkibnVtYmVyIiA6IFswLDRdCgoJCX0sCgoJCXsKCQkJInBvc2l0aW9uIiA6ICJBIGRpZmZlcmVudCBwb3NpdGlvbiBhbGxvd2luZyBvbmUgY2hvaWNlIiwKCQkJIm9wdGlvbnMiIDogWwoJCQkJIk9uZSBvcHRpb24iLAoJCQkJIlRoZSBvdGhlciBvcHRpb24iCgkJCV0sCgkJCSJudW1iZXIiIDogWzEsMV0KCQl9CgldCn0="]'
        start(test_str)

    elif args[1] == "test_vote" and len(args) == 2:
        test_str = '["election","ewogICAgInNlbGVjdGlvbnMiOiBbCiAgICAgICAgewogICAgICAgICAgICAicG9zaXRpb24iOiAiV2hhdCBpcyBiZWluZyBzZWxlY3RlZCAiLAogICAgICAgICAgICAidm90ZSI6IFsiT25lIG9wdGlvbiIsICJBIHRoaXJkIG9wdGlvbiJdCiAgICAgICAgfSwKICAgICAgICB7CiAgICAgICAgICAgICJwb3NpdGlvbiI6ICJBIGRpZmZlcmVudCBwb3NpdGlvbiBhbGxvd2luZyBvbmUgY2hvaWNlIiwKICAgICAgICAgICAgInZvdGUiOiBbIlRoZSBvdGhlciBvcHRpb24iXQogICAgICAgIH0KICAgIF0KfQ=="]'
        vote(test_str)

    elif args[1] == "test_stop" and len(args) == 2:
        stop('["election"]')

    else:
        help()

if __name__== "__main__":
    main()
