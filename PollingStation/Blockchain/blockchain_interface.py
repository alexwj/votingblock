#!/usr/bin/env python3

from sys import argv as args
from subprocess import Popen as run

def help():
    print("Usage: " + args[0] + " COMMAND")
    print("\tCommands:")
    print("\t\tstart - starts a new node")
    print("\t\trestart - starts a previously started node")
    print("\t\tstop - stops the currently running node")
    print("\t\tclear - deletes node data")
    print("\t\treset - combination of stop and clear")
    print("\t\tload - load the contract on the node")
    exit(1)

def start():
    return run(["scripts/start_node.sh"])

def restart():
    return run(["scripts/restart_node.sh"])

def stop():
    return run(["scripts/stop_node.sh"])

def clear():
    return run(["scripts/clear_node.sh"])

def load():
    return run(["scripts/load_contract.sh"])

def main():
    if len(args) != 2:
        help()

    if args[1] == "start":
        start()

    elif args[1] == "restart":
        restart()

    elif args[1] == "stop":
        stop()

    elif args[1] == "clear":
        clear()

    elif args[1] == "reset":
        stop()
        clear()
    
    elif args[1] == "load":
        load()
 
    else:
        help()

if __name__== "__main__":
    main()
