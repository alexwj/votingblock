#!/bin/bash

#A setup script for Ubuntu 18.04

wget https://github.com/eosio/eos/releases/download/v1.5.0/eosio_1.5.0-1-ubuntu-18.04_amd64.deb
sudo apt install ./eosio_1.5.0-1-ubuntu-18.04_amd64.deb
