#!/bin/bash

WALLET=http://127.0.0.1:9050
SERVER=http://127.0.0.1:9051

cleos --url $SERVER --wallet-url $WALLET push action election vote $1 -p election@active
