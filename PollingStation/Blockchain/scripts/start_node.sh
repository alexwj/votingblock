#!/bin/bash

keosd --http-server-address 127.0.0.1:9050 &

echo "keosd started on 9050"

nodeos -e -p eosio \
--plugin eosio::producer_plugin \
--plugin eosio::chain_api_plugin \
--plugin eosio::http_plugin \
--plugin eosio::history_plugin \
--plugin eosio::history_api_plugin \
--access-control-allow-origin='*' \
--contracts-console \
--http-validate-host=false \
--verbose-http-errors \
--http-server-address 127.0.0.1:9051 \
--filter-on='*' >> nodeos.log 2>&1 &

echo "nodeos started on 9051"
