#!/bin/bash

#Server and wallet urls
WALLET=http://127.0.0.1:9050
SERVER=http://127.0.0.1:9051
DEVKEY=5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3
CONTRACTS=/home/alex/Documents/homework/4284/votingblock/PollingStation/Blockchain/contracts/election

#Create the wallet
cleos --url $SERVER --wallet-url $WALLET wallet create --file wallet.dat
cleos --url $SERVER --wallet-url $WALLET wallet open -n default
cleos --url $SERVER --wallet-url $WALLET wallet unlock -n default < wallet.dat

#Create a key
PUBKEY=$(cleos -u http://127.0.0.1:9051 --wallet-url http://127.0.0.1:9050 wallet create_key | cut -d '"' -f 2)

#Import the unsafe developer key
cleos --url $SERVER --wallet-url $WALLET wallet import --private-key $DEVKEY

#Create the election account
cleos --url $SERVER --wallet-url $WALLET create account eosio election $PUBKEY

#Uploads the contract cleos set contract hello ls/hello -p hello@active
cleos --url $SERVER --wallet-url $WALLET set contract election $CONTRACTS -p election@active
