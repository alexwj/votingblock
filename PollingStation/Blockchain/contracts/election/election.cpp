#include <eosiolib/eosio.hpp>
#include "base64.hpp"
#include "json.hpp"

#define DEBUG 1

using namespace eosio;
using namespace nlohmann;

class [[eosio::contract("election")]] election : public contract {
    public:

        using contract::contract;

        election(name receiver, name code,  datastream<const char*> ds):
            contract(receiver, code, ds) {}

        /*
            Election start action
            Takes user name and base64 encoded JSON string
        */
        [[eosio::action]]
        void start(name user, std::string arg_b64)
        {
            require_auth(user);

            if(DEBUG)
            {
                print("Election action received!");
            }

            election_index elections(_code, _code.value);
            
            auto iterator = elections.find(user.value);
            
            eosio_assert(iterator == elections.end(),
                "Election already running for this user!");
            
            elections.emplace(user, [&](auto& row) {
                row.key = user;
                row.state = handle_start(arg_b64);
            });
        }

        /*
            Election stop action
            Takes user name
        */
        [[eosio::action]]
        void stop(name user)
        {
            require_auth(user);

            if(DEBUG)
            {
                print("Stop action received!");
            }
            
            election_index elections(_code, _code.value);
            
            auto iterator = elections.find(user.value);
            
            eosio_assert(iterator != elections.end(), "No such election!");
            
            elections.modify(iterator, user, [&](auto& row) {
                print(json::parse(row.state).dump());
            });
            
            elections.erase(iterator);
        }

        /*
            Election vote action
            Takes user name and base64 encoded JSON string
        */
        [[eosio::action]]
        void vote(name user, std::string arg_b64)
        {
            require_auth(user);

            if(DEBUG)
            {
                print("Vote action received!");
            }

            election_index elections(_code, _code.value);

            auto iterator = elections.find(user.value);

            eosio_assert(iterator != elections.end(), "No such election!"); 

            elections.modify(iterator, user, [&]( auto& row ) {
                row.state = handle_vote(row.state, arg_b64);
            });
        }

        /*
            Table to store election data
        */
        struct [[eosio::table]] electiondata
        {
            name key;
            std::string state;
            uint64_t primary_key() const { return key.value; }
        };

        typedef eosio::multi_index<"state"_n, electiondata> election_index;
    
    private:

        /*
            Function to set the election state after the start action
            Takes base64 encoded JSON string
        */
        std::string handle_start(std::string arg_b64)
        {
            json arg = json::parse(base64_decode(arg_b64));

            json state = json::object();

            json choices = json::array();

            for(auto& choice : arg["choices"].items())
            {
                json options = json::array();

                for(auto& option : choice.value()["options"].items())
                {
                    json option_obj = json::object();
                   
                    option_obj.emplace("name", option.value());    
     
                    option_obj.emplace("count", 0);
       
                    options.emplace_back(option_obj);
                }

                json choice_obj = json::object();
                
                choice_obj.emplace("options", options);
                choice_obj.emplace("position", choice.value()["position"]);
                choice_obj.emplace("number", choice.value()["number"]);

                choices.emplace_back(choice_obj);
            }
            
            state.emplace("choices", choices);
            state.emplace("election_title", arg["election_title"]);

            return state.dump();
        }
        
        /*
            Function to set the election state after the stop action
            Takes a JSON string
        */
        std::string handle_stop(std::string state_str)
        {
            json state = json::parse(state_str);

            //Modify state here

            return state.dump();
        }

        /*
            Function to set the election state after a vote action
            Takes a JSON string and base64 encoded JSON string
        */
        std::string handle_vote(std::string state_str, std::string arg_b64)
        {
            json state = json::parse(state_str);
            json arg = json::parse(base64_decode(arg_b64));
  
            json choices = json::array();
 
            for(auto& choice : state["choices"].items())
            {
                json choice_obj = choice.value();
                
                json position = choice_obj["position"];
                json bounds = choice_obj["number"];
                
                int min = bounds[0];
                int max = bounds[1];

                bool found = false;

                json options = json::array();
                
                for(auto& selection : arg["selections"].items())
                {
                    json selection_obj = selection.value();
                    
                    json selection_pos = selection_obj["position"];
            
                    if(!position.dump().compare(selection_pos.dump()))
                    {
                        found = true;

                        json selection_votes = selection_obj["votes"];
                        
                        int vote_count = 0;

                        for(auto& option : choice_obj["options"].items())
                        {
                            json option_name = option.value()["name"];

                            for(auto& vote : selection_obj["vote"].items())
                            {
                                std::string vote_name = vote.value().dump();

                                if(!option_name.dump().compare(vote_name))
                                {
                                    int old = option.value()["count"];
                                    option.value()["count"] = old + 1;
                                    vote_count++;
                                }
                            }

                            options.emplace_back(option.value());
                        }
        
                        eosio_assert(min <= vote_count, "#selections < min");
                        eosio_assert(max >= vote_count, "#selections > max");

                        break;   
                    }
                }

                eosio_assert(found, "Bad voting data");

                choice_obj.erase("options");
    
                choice_obj.emplace("options", options);

                choices.emplace_back(choice_obj);
            }

            state.erase("choices");

            state.emplace("choices", choices);

            return state.dump();
        }
};

EOSIO_DISPATCH(election, (start) (stop) (vote))
