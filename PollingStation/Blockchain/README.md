# VotingBlock
## Blockchain Node Documentation

### Overview

The following code sets up an EOSIO node and loads a smart contract to it to conduct the election.

`./blockchain_interface` is a Python script for starting and stopping the node.

`./action_interface` is a Python script for testing the node.

The `./scripts` folder contains Bash scripts called by `./blockchain_inteface` and `./action_interface`.

The `./contracts` folder contains the files for the election smart contract.

### General Usage Information

#### Starting the Node
`./blockchain_interface reset`

`./blockchain_interface start`

#### Load the Smart Contract

The smart contract is loaded from the absolute path in the CONTRACTS variable found in `./scripts/load\_contract.sh`.

`./blockchain_inteface load`

#### Starting the Election

Before votes can be cast, the `start` action must be used to start the election.  The `start` action takes two arguments, an account name, 'election', and a base64 encoded JSON string of the following format:

````javascript
{
	"election_title" : "Human Readable Title",
	"choices" : [
		{
			"position" : "What is being selected ",
			"options" : [
				"One option",
				"Another option",
				"A third option"
			],
			"number" : [0,2]

		},

		{
			"position" : "A different position allowing one choice",
			"options" : [
				"One option",
				"The other option"
			],
			"number" : [1,1]
		}
	]
}
````

#### Voting in the Election

Once the election is started, the `vote` action may be used to cast votes in the election.  The `vote` action takes two arguments, and account name, 'election', and a base64 encoded string of the following format:

````javascript
{
    "selections": [
        {
            "position": "What is being selected ",
            "vote": ["One option", "A third option"]
        },
        {
            "position": "A different position allowing one choice",
            "vote": ["The other option"]
        }
    ]
}
````

#### Retrieving the Results

Election results can be retrieved with eosjs's `get_table_rows` function.  The primary key for the table will be 'election', and the results will be stored in a JSON string called 'state' in the following format:

````javascript
{
    "choices": [
        {
            "number": [0, 2],
            "options": [
                {
                    "count": 1,
                    "name": "One option"
                },
                {
                    "count": 0,
                    "name": "Another option"
                },
                {
                    "count": 1,
                    "name": "A third option"
                }
            ],
            "position": "What is being selected "
        },
        {
            "number": [1, 1],
            "options": [
                {
                    "count": 0,
                    "name": "One option"
                },
                {
                    "count": 1,
                    "name": "The other option"
                }
            ],
            "position": "A different position allowing one choice"
        }
    ],
    "election_title": "Human Readable Title"
}
````

#### Stopping the Election

Once the election is done, and the results have been retrieved, the election contract can be stopped with the `stop` action.  The only argument taken by the stop action is the account name, 'election'.

### Advanced Usage Information

This section is still under construction.

#### Node Information

The default port for keosd is port 9050, and the default port for nodeos is 9051.  These ports can be modified in the `./scripts` folder.

#### Smart Contract Information

The smart contract is defined in `./contracts/election/election.cpp`, and can be re-built with the Makefile found in that directory.  Licensing info for the header files used in the smart contract can be found in the relevent header files. 
