//Written by Alexander Mundy
//Middleware server for VotingBlock system

//Performs imports required for EOSJS
const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');

//Gets election configuration data from ./election.json
const fs = require('fs');
let rawdata = fs.readFileSync('election.json');
const electionConfig = JSON.parse(rawdata);
console.log("Election config: " + JSON.stringify(electionConfig) + "\n\n"); 

//Configures the web server
const http = require('http');
const port = 8080;
const hostname = '127.0.0.1';
var numRequests = 0;

//Initializes objects needed for EOSJS
const defaultPrivateKey = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"; // eosio dev key
const signatureProvider = new JsSignatureProvider([defaultPrivateKey]);
const rpc = new JsonRpc('http://127.0.0.1:9051', { fetch });
const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

//Sends the start command to the blockchain
var setup64 = Buffer.from(JSON.stringify(electionConfig), 'binary').toString("base64");
console.log("setup64: " + setup64);
(async () => {
	try{
	const result = await api.transact({
		actions: [{
			account: 'election',
			name: 'start',
			authorization: [{
				actor: 'eosio',
				permission: 'active',
			}],
			data: {
				user: 'eosio',
				arg_b64: setup64
			}	
		}]
	}, {
			blocksBehind: 3,
			expireSeconds: 30,
		});
	console.dir(result);
	}
	catch (error){
		console.error(error);
	}


})();

//Initializes the vote verifier
const verifier = require('./verify_vote_json.js');

//Runs the server
const server = http.createServer(function(request, response) {
	numRequests = numRequests + 1;
	console.log('Request #' + numRequests);
	//console.dir(request.param)
	
	//Deals with POST requests, which should be votes
	if (request.method == 'POST') {
		console.log('POST');
		var body = ''
		var contentType = request.headers['content-type'];

    		request.on('data', function(data) {
      			body += data
    		})

		request.on('end', function() {
			console.log('Body: ' + body)
			response.writeHead(200, {'Content-Type': 'text/html'})
			console.log("Content Type: " + contentType);
			//Should only accept json
			if(contentType == 'application/json; charset=UTF-8'){
				response.end('JSON recieved:\n' + body + '\n\n');
				console.log('JSON received');
				parseVote(body);
			}	
			//Reject any other POSTs
			else{
				response.end('POST received:\n' + body + '\n\n');
				return;
			}
    		})

		//Parses votes received
		function parseVote(voteBody){
			console.log("Attempting to parse vote");

			//Attempts to convert the body of the received POST into a JSON
			try{
				var jsonObj = JSON.parse(JSON.stringify(voteBody));
				//console.log("jsonObj: " + jsonObj)
				//console.log("Verify vote: " + verifier.verify_vote_json(jsonObj));
			}
			catch(error){
				console.error(error)
				return;
			}

			//Encodes the JSON in base64, which is required by the backend
			var jsonTo64 = new Buffer.from(jsonObj, 'binary').toString("base64");

			//Anonymous function that performs the transaction
			(async () => {
				try{
					var lastJson = {
						actions: [{
							account: 'election',
							name: 'vote',
							authorization: [{
								actor: 'eosio',
								permission: 'active',
							}],
							data: {
								user: 'eosio',
								arg_b64: jsonTo64
							}	
						}]
					};
					console.log("lastJson: " + JSON.stringify(lastJson));

					const result = await api.transact(
						lastJson, 
						{
							blocksBehind: 3,
							expireSeconds: 30,
						});
					console.dir(result);
				}
				catch (error){
					console.error(error);
				}

		
			})();
		}

	} else {
    		console.log('GET: ' + request.url);
		if(request.url == "/"){
			response.writeHead(200, {'Content-Type': 'application/json'})
			response.end(JSON.stringify(electionConfig));
		}
		else if(request.url == "//results"){
			console.log("Attempting to get results");
			(async () => {
				try{
					var electionResults = await rpc.get_table_rows({
						json: true,
						code: 'election',
						scope: 'election',
						table: 'state',
						limit: 10,
					});
					console.log(electionResults.rows);

					response.writeHead(200, {'Content-Type': 'application/json'});

					//console.log("type: " + typeof electionResults.rows);
					var jsonOut = electionResults.rows[0].state;
					//console.log("type: " + typeof jsonOut);
					//console.log("jsonOut: " + jsonOut);
					response.end(jsonOut);
				}
				catch (error){
					console.error(error);
					response.writeHead(500, {'Content-Type': 'text/plain'});
					response.end("Failed to retrieve elecion results");
				}
			})();
		}
		else{
			response.writeHead(418, {'Content-Type': 'text/plain'});
			response.end("I'm sorry, we don't have that page");
		}
	}

	console.log('\n\n');
});

server.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`);
});
