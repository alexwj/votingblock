# Voting Block
## Middleware Server Documentation

### Overview
This middleware server manages access to the blockchain backend and configures elections.

`./README.md` is this file.

`./app.js` runs the server, accepting GET and POST requests from the frontend.

`./election.json` holds the configuration for the current election in JSON format.

`./serverLog` is where debug and error logs from app.js are dumped. Created on first run.

`./setup.py` is a CLI tool to conveniently configure elections. It builds `./election.json`.

`./start_server.py` starts the server.

`./verify_vote_json.js` is a script that verifies that the vote received is valid.


### General Usage Information

#### Configuring an Election
Run `python3 ./setup.py` to configure the election. This will launch a CLI tool to build the `./election.json' file.
The user will be prompted to enter the election title, the number of positions in the election, the title of each position, and the candidates for each position.
IMPORTANT: Backspace is not supported in the tool, so type carefully.

#### Installation
Run `sudo npm install node-fetch eosjs` to install dependencies.

#### Starting the Server
Run `./start_server.py` to start the server.

The blockchain backend MUST be started before starting the middleware server.
No other instance of the server may be running at the same time.
On rare occasions, the election configuration will timeout. If this occurs, kill the server by PID, then start it again.
Changes to the election configuration will require reseting both the middleware server and the blockchain backend.

#### Stopping the Server
Currently, the only way to stop the server is via 'kill \<PID\>'.


### Network Setup
The server is hosted on port 8080. http://\<servername\>/api must be redirected by a reverse proxy to port 8080.
Votes received are sent to 127.0.0.1:9051 to be entered into the blockchain via EOSJS's JsonRPC.
