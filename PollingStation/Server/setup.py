import json

data = {}
data['election_title'] = input("What is the title of the election?\n")

data['choices'] = []
num_selections = int(input("How many different positions will there be?\n"))
for i in range(num_selections):
    suboption = {}
    suboption['position'] = input("What is the title of position %s\n" % i)

    suboption['options'] = []
    print("What are the available options? Separate each choice with a newline and enter a blank line when you're done.")
    x = "none"
    while x:
        x = input()
        if x:
            suboption['options'].extend([x])

    min = input("Minimum number of choices?\n")
    max = input("Maximum number of choices?\n")
    suboption['number'] = [int(min), int(max)]

    data['choices'].extend([suboption])

json_data = json.dumps(data)
print(json_data)
f = open('election.json', 'w')
f.write(json_data)
f.close()
