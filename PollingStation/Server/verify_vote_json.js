module.exports = {

	verify_vote_json : function (json_obj) {
	    if (/*!json_obj.hasOwnProperty('election_title') || */!json_obj.hasOwnProperty('selections')) {
		// If it doesn't have a selections key or a election_title key
		console.log("Missing 'selections'");
		return false;
	    }

	    if (Object.keys(json_obj).length != 2) {
		// If it doesn't have only two keys
		console.log("Wrong number of keys")
		return false;
	    }

	    if (typeof json_obj['election_title'] != "string") {
		// If the election title is not a string
		return false;
	    }

	    n = Object.keys(json_obj['selections']).length;
	    for (i = 0; i < n; i++) {
		temp = json_obj['selections'][i];
		if (!temp.hasOwnProperty('position') || !temp.hasOwnProperty('options') || !temp.hasOwnProperty('number')) {
		    // If it doesn't have one of the three required keys
		    return false;
		}
		if (Object.keys(temp).length != 3) {
		    // If it has more than three keys
		    return false;
		}
		if (typeof temp['position'] != "string" || typeof temp['options'] != "object" || typeof temp['number'] != "object") {
		    // If the keys aren't the right object types
		    // The type checking is weird because arrays show up as objects rather than some kind of array
		    return false;
		}
	    }

	    return true;
	}
};
