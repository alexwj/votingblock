import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Logo from './logo';

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
	appBar: {
		position: 'relative',

	},
	logo: {
		height: '48px',
		width: '48px',
		marginLeft: 'auto',
		marginRight: 'auto',
		[theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
			marginLeft: theme.spacing.unit * 6,
		},
	},
	layout: {
		width: 'auto',
		marginLeft: theme.spacing.unit * 2,
		marginRight: theme.spacing.unit * 2,
		[theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
			width: 600,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 3,
		marginBottom: theme.spacing.unit * 3,
		padding: theme.spacing.unit * 2,
		[theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
			marginTop: theme.spacing.unit * 6,
			marginBottom: theme.spacing.unit * 6,
			padding: theme.spacing.unit * 3,
		},
	},
	footer: {
		backgroundColor: theme.palette.background.paper,
		padding: theme.spacing.unit * 6,
	},
});

class Page extends Component {
	render() {
		const {classes, component} = this.props;

		return (
			<Fragment>
				<CssBaseline />
				<AppBar color='default' position='static' className={classes.appBar}>
					<Toolbar>
						<Logo className={classes.logo} />
					</Toolbar>
				</AppBar>
				<main className={classes.layout}>
					<Paper className={classes.paper}>
						{component}
					</Paper>
				</main>
				<footer className={classes.footer}>
					<Typography variant='h6' align='center' gutterBottom>
						VotingBlock &copy; 2019. All rights reserved.
					</Typography>
					<Typography variant='subtitle1' align='center' color='textSecondary' component='p'>
						{'Powered by '}
						<Link href={'https://www.eos.io'} className={classes.link}>
							EOS.io
						</Link>
					</Typography>
				</footer>
			</Fragment>
		);
	}
}

Page.propTypes = {
	classes: PropTypes.object.isRequired,
	component: PropTypes.object.isRequired,
};

export default withStyles(styles)(Page);
