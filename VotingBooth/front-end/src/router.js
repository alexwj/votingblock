import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import VoteApp from './vote/voteapp';
import ResultsApp from './results/resultsapp';

class AppRouter extends Component {
	render() {
		return (
			<Router>
				<Route path='/' exact component={VoteApp} />
				<Route path='/results' exact component={ResultsApp} />
			</Router>
		);
	}
}

export default AppRouter;
