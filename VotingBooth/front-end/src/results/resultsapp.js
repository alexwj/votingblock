import React, { Component } from 'react';
import Results from './results';
import Page from '../common/page';

class ResultsApp extends Component {
	render() {
		return (
			<Page
				component={(<Results />)}
			/>
		);
	}
}

export default ResultsApp;
