import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {HorizontalBar} from 'react-chartjs-2';

const styles = theme => ({
	title: {
		padding: theme.spacing.unit * 2,
	},
});

class Results extends Component {
	constructor(props) {
		super(props);

		this.state = {
			'electionData': {
				'choices': []
			}
		};
	}

	componentDidMount() {
		const url = '/api/results';
		const request = new XMLHttpRequest();
		const component = this;
		request.onload = function() {
			const data = JSON.parse(this.responseText);
			component.setState({'electionData': data});
		};
		request.open('GET', url, true);
		request.send();
	}

	render() {
		const {classes} = this.props;

		const barCharts = this.state.electionData.choices.map(choice => {
			const data = {
				labels: [],
				datasets: [{
					backgroundColor: [],
					borderColor: [],
					borderWidth: 1,
					data: [],
					label: '# of votes',
				}],
			};

			const options = {
				responsive: true,
				legend: {
					display: false,
				},
				animation: {
					animateScale: true
				},
				scales: {
					xAxes: [{
						ticks: {
							fontFamily: 'inherit',
							beginAtZero: true,
							callBack: function(value) {
								if (Number.isInteger(value))
									return value;
							},
							stepSize: 1,
						}
					}]
				}
			};

			const color = (index, alpha) => (
				[
					'rgba(255, 99, 132, ',
					'rgba(54, 162, 235, ',
					'rgba(255, 206, 86, ',
					'rgba(75, 192, 192, ',
				][index % 4] + alpha + ')'
			);

			choice.options.forEach((option, index) => {
				data['labels'].push(option.name);
				data['datasets'][0]['data'].push(option.count);
				data['datasets'][0]['backgroundColor'].push(color(index, 0.2));
				data['datasets'][0]['borderColor'].push(color(index, 1));
			});

			return (
				<Fragment key={choice.position}>
					<Typography
						className={classes.title}
						component='h2' variant='h6' align='left'>
						{choice.position}
					</Typography>
					<HorizontalBar
						data={data}
						options={options}
					/>
				</Fragment>
			);
		});

		return (
			<Fragment>
				<Typography component='h2' variant='h5' align='left'>
					{this.state.electionData.election_title} Results
				</Typography>
				{barCharts}
			</Fragment>
		);
	}
}

Results.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Results);
