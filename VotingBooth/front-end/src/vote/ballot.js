import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import ResponseBox from './response-box';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

const styles = theme => ({
	buttonBox: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing.unit * 3,
		marginLeft: theme.spacing.unit,
	},
});

class Ballot extends Component {
	render() {
		const {classes, election, missing, makeSelectionChangeHandler, onSubmit, activeStep} = this.props;

		if (activeStep !== 0) {
			return null;
		}

		const steps = ['Cast vote', 'Preview results'];

		const responseBoxes = election.choices.map((selection, index) => {
			return (
				<Fragment key={index}>
					<ResponseBox
						position={selection.position}
						options={selection.options}
						checked={selection.checked}
						missing={missing && missing.includes(selection.position)}
						onSelectionChange={makeSelectionChangeHandler(index)}
					/>
					{index === election.choices.length - 1 ? null : <Divider variant="middle" />}
				</Fragment>
			);
		});

		return (
			<Fragment>
				<Typography component='h2' variant='h5' align='left'>
					{election.election_title}
				</Typography>
				<Stepper activeStep={activeStep} className={classes.stepper}>
					{steps.map(label => (
						<Step key={label}>
							<StepLabel>{label}</StepLabel>
						</Step>
					))}
				</Stepper>
				{responseBoxes}
				<div className={classes.buttonBox}>
					<Button
						variant="contained"
						color="primary"
						onClick={onSubmit}
						className={classes.button}
					>
						Preview Vote
					</Button>
				</div>
			</Fragment>
		);
	}
}

Ballot.propTypes = {
	classes: PropTypes.object.isRequired,
	election: PropTypes.object.isRequired,
	makeSelectionChangeHandler: PropTypes.func.isRequired,
	onSubmit: PropTypes.func.isRequired,
	missing: PropTypes.array,
	activeStep: PropTypes.number,
};

export default withStyles(styles)(Ballot);
