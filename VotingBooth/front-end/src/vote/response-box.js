import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';

const styles = theme => ({
	formControl: {
		margin: theme.spacing.unit * 3,
	},
});

class ResponseBox extends Component {
	render() {
		const {classes, position, options, checked, missing, onSelectionChange} = this.props;

		const choices = options.map((option, i) => (
			<FormControlLabel
				key={position + '_' + option}
				value={option}
				control={<Radio color="primary" checked={checked[i]} />}
				label={option}
			/>
		));
		return (
			<Fragment>
				<FormControl error={missing} component="fieldset" className={classes.formControl}>
					<FormLabel component="legend">{position}</FormLabel>
					<RadioGroup
						aria-label={position}
						name={position}
						className={classes.group}
						onChange={onSelectionChange}
					>
						{choices}
					</RadioGroup>
					{this.props.missing &&
						<FormHelperText>Please make a selection for {this.props.position}.</FormHelperText>
					}
				</FormControl>
			</Fragment>
		);
	}
}

ResponseBox.propTypes = {
	classes: PropTypes.object.isRequired,
	position: PropTypes.string.isRequired,
	options: PropTypes.array.isRequired,
	checked: PropTypes.array.isRequired,
	onSelectionChange: PropTypes.func.isRequired,
	missing: PropTypes.bool,
};


export default withStyles(styles)(ResponseBox);
