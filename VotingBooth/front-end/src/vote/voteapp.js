import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Ballot from './ballot';
import Preview from './preview';
import Page from '../common/page';

class VoteApp extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activeStep: 0,
			missing: [],
			election: {
				election_title: '',
				choices: [],
			}
		};

		this.makeSelectionChangeHandler = this.makeSelectionChangeHandler.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleBack = this.handleBack.bind(this);
		this.handleAdvance = this.handleAdvance.bind(this);
	}

	makeSelectionChangeHandler(selectionIndex) {
		return function (event) {
			const target = event.target;
			const value = target.value;
			const name = target.name;
			const election = this.state.election;
			const selection = election.choices[selectionIndex];
			selection.checked = selection.checked.map((p, i) => value === selection.options[i]);

			this.setState({[name]: value});
			this.setState({'election': election});
		}.bind(this);
	}

	handleBack() {
		this.setState({activeStep: 0});
	}

	handleAdvance(event) {
		const response = {};
		const state = this.state;
		const missing = [];

		response.selections = state.election.choices.map(function(selection) {
			const vote = typeof state[selection.position] === 'string'
				? [state[selection.position]] : [];

			if (selection.required && vote.length === 0) {
				missing.push(selection.position);
			}

			return {'position': selection.position, 'vote': vote};
		});

		if (missing.length === 0) {
			this.setState({activeStep: 1});
		} else {
			this.setState({missing: missing});
		}

		event.preventDefault();
	}

	handleSubmit(event) {
		const response = {};
		const state = this.state;
		const missing = [];

		response.selections = state.election.choices.map(function(selection) {
			const vote = typeof state[selection.position] === 'string'
				? [state[selection.position]] : [];

			if (selection.required && vote.length === 0) {
				missing.push(selection.position);
			}

			return {'position': selection.position, 'vote': vote};
		});

		if (missing.length === 0) {
			if (postVote(response)) {
				this.setState({activeStep: 2});
				this.props.history.push('/results');
			}
		} else {
			this.setState({missing: missing});
			this.setState({activeStep: 0});
		}
		event.preventDefault();
	}

	componentDidMount() {
		const url = '/api';
		const request = new XMLHttpRequest();
		const component = this;
		request.onload = function() {
			const election = JSON.parse(this.responseText);
			election.choices.forEach((selection) => {
				selection.checked = selection.options.map(()=>false);
			});
			component.setState({'election': election});
		};
		request.open('GET', url, true);
		request.send();
	}

	render() {
		const {activeStep, missing, election} = this.state;
		const votes = this.state.election.choices.map(s => this.state[s.position] || '');

		let PageComponent = null;
		if (activeStep === 0) {
			PageComponent = (
				<Ballot
					election={election}
					makeSelectionChangeHandler={this.makeSelectionChangeHandler}
					onSubmit={this.handleAdvance}
					missing={missing}
					activeStep={activeStep}
				/>
			);
		} else {
			PageComponent = (
				<Preview
					election={election}
					votes={votes}
					onSubmit={this.handleSubmit}
					onBack={this.handleBack}
					activeStep={activeStep}
				/>
			);
		}

		return (
			<Page
				component={PageComponent}
			/>
		);
	}
}

function postVote(data) {
	const request = new XMLHttpRequest();
	request.open('POST', '/api', true);
	request.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
	request.send(JSON.stringify(data));
	return true;
}

VoteApp.propTypes = {
	history: PropTypes.object.isRequired,
	location: PropTypes.object.isRequired,
};

export default (VoteApp);
