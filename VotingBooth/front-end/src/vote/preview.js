import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

const styles = theme => ({
	buttonBox: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing.unit * 3,
		marginLeft: theme.spacing.unit,
	},
});

class Preview extends Component {
	render () {
		const {classes, election, votes, onSubmit, onBack, activeStep} = this.props;

		if (activeStep !== 1) {
			return null;
		}

		const steps = ['Cast vote', 'Preview results'];

		const display = election.choices.map((selection, index) => {
			const text = votes[index]
				? (<>You selected <b>{votes[index]}</b> for {selection.position}.</>)
				: (<>You made no selection for {selection.position}</>);

			return (
				<Fragment key={index}>
					<Typography component="h6">
						{text}
					</Typography>
				</Fragment>
			);
		});
		return (
			<Fragment>
				<Typography component='h2' variant='h5' align='left'>
					{election.election_title}
				</Typography>
				<Stepper activeStep={activeStep} className={classes.stepper}>
					{steps.map(label => (
						<Step key={label}>
							<StepLabel>{label}</StepLabel>
						</Step>
					))}
				</Stepper>
				<Typography component="h3" variant="h6" align="center">
					Please confirm your vote:
				</Typography>
				{display}
				<div className={classes.buttonBox}>
					<Button
						variant="contained"
						color="secondary"
						onClick={onBack}
						className={classes.button}
					>
						Change Vote
					</Button>
					<Button
						variant="contained"
						color="primary"
						onClick={onSubmit}
						className={classes.button}
					>
						Vote
					</Button>
				</div>
			</Fragment>
		);
	}
}

Preview.propTypes = {
	classes: PropTypes.object.isRequired,
	election: PropTypes.object.isRequired,
	votes: PropTypes.array.isRequired,
	onSubmit: PropTypes.func.isRequired,
	onBack: PropTypes.func.isRequired,
	activeStep: PropTypes.number.isRequired,
};

export default withStyles(styles)(Preview);
