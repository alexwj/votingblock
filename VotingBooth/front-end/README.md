# VotingBlock
## Front End Documentation

Voting Block is a distributed voting platform aimed to improve the availability 
of fast, secure, and private voting to Virginia Tech student organizations.

### Usage

To participate in an election navigate to http://capstone.johnson.engineer/
Make selections and hit the 'preview' button at the bottom right-hand side of 
the form, the next dialogue will prompt you to confirm you vote. After voting
you will be redirected to the election results page.

To view election results without voting navigate to 
http://capstone.johnson.engineer/results

### Setup

Inside the project, you can run some built-in commands:
#### `npm start` or `yarn start`

Runs the app in development mode.<br>
Open [http://capstone.johnson.engineer:3000](http://localhost:3000) to view it in the browser.

The page will automatically reload if you make changes to the code.<br>
You will see the build errors and lint warnings in the console.

#### `npm run build` or `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>

To deploy, copy the `votingblock/VotingBooth/front-end/build/` folder to your static web root.
For example, if using Apache copy `votingblock/VotingBooth/front-end/build/` to `/var/www/html`.
