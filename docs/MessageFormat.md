# Format for candidate list

```json
{
    "election_title": "a titular string",
    "selections": [
        {
            "position": "What is being selected",
            "options": [
                "One selection",
                "Another selection"
            ],
            "number": [0, 1]
        },
        {
            "position": "A different position allowing more than one selection",
            "options": [
                "Red",
                "Blue",
                "Green",
                "Purple",
            ],
            "number": [1, 4]
        }
    ]
}
```

`selections` is a list of item to vote on, each is an object with a position title, the list of options, and a range for the count of options that must be chosen (inclusive).

# Format for vote

For each item in the candidate list `selections` array, there is a vote object with the position name and an array of the selected options. The array must contain the correct number of elements as specified by the `number` field in the candidate list.

```json
{
    "selections": [
        {
            "position": "The name of the position",
            "vote": ["the voted for option"]
        },
        {
            "position": "The name of the position",
            "vote": ["some elections might", "allow multiple selections"]
        }
    ]
}
```
